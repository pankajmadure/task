const express = require('express')
const app = express()
const port = 4000
const mongoose = require('mongoose')
const bodyParser = require('body-parser');
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});
app.use(bodyParser.json());

const userDetails = require('./models/userDetails');
const excelInfo = require('./models/excelinfo');
const connectionString = 'mongodb+srv://user_test:12345@cluster0-w5qdh.mongodb.net/test?retryWrites=true&w=majority'
mongoose.connect(connectionString)
    .then(() => {
        console.log("Succesfully connected to MongoDB Atlas");
    })
    .catch((error) => {
        console.log("Unable to connect to mongo", error)
    })
app.get('/', (req, res) => res.send('Hello World!'))


app.post('/api/saveExcelInfo/:username/:password', (req, res, next) => {
    const excelinfo = new excelInfo({
        username: req.params.username,
        password: req.params.password,
        filename: req.body.filename,
        rows: req.body.rows,
        cols: req.body.cols
    })
    excelinfo.save().then(
        () => {
            res.status(201).json({
                message: "excel data saved succesfully"
            })
        }
    ).catch((error) => {
        res.status(400).json({
            error: error
        })
    })
})
app.get('/api/getExcelInfo/:username/:password', (req, res, next) => {
    
    excelInfo.find({ username: req.params.username, password: req.params.password }).then(
        (saveExcelDatabyId) => {
            if (saveExcelDatabyId != null)
                res.status(200).json(saveExcelDatabyId)
            else
                res.status(404).json({
                    message: "No excel data is stored"
                })
        }
    ).catch(
        (err)=>{
            console.log("error",err)
        }
        )
})
app.post('/api/saveUser', (req, res, next) => {
    const userdetails = new userDetails({
        username: req.body.username,
        password: req.body.password,
        name: req.body.name,
        mobileNumber: req.body.mobileNumber
    });
    userdetails.save().then(
        () => {
            res.status(201).json({
                message: 'user details saved succesfully'
            })
        }
    ).catch((error) => {
        res.status(400).json({
            error: error
        });
    })
})
app.get('/api/getUser', (req, res, next) => {
    userDetails.find().then(
        (userdetails) => {
            res.status(200).json(userdetails)
        }).catch(
            (error) => {
                res.status(400).json({
                    error: error
                })
            }
        )
})
app.get('/api/getUser/:username/:password', (req, res, next) => {
    userDetails.findOne({ username: req.params.username, password: req.params.password }).then(
        (userdetailbyid) => {
            if (userdetailbyid != null)
                res.status(200).json(userdetailbyid)
            else
                res.status(404).json({
                    message: "User does not exist"
                })
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            })
        }
    )
})

app.put('/api/saveUser/:id', (req, res, next) => {
    const userdetails = new userDetails({
        _id: req.params.id,
        username: req.body.username,
        password: req.body.password,
        name: req.body.name,
        mobileNumber: req.body.mobileNumber
    });
    userDetails.updateOne({ _id: req.params.id }, userdetails).then(
        () => {
            res.status(201).json({
                message: "User details updated successfully"
            })
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            })
        }
    )
})

app.delete('/api/deleteUser/:id', (req, res, next) => {
    userDetails.deleteOne({ _id: req.params.id }).then(
        () => {
            res.status(200).json({
                message: "User  deleted"
            })
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            })
        }
    )
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))

