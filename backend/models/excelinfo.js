const mongoose = require('mongoose');
const excelInfoSchema = mongoose.Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    filename: { type: String, required: true },
    rows: { type: Array, required: true },
    cols: { type: Array, required: true }
});
module.exports = mongoose.model('excelInfo', excelInfoSchema);