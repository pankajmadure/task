const mongoose= require('mongoose');
const userDetailsSchema=mongoose.Schema({
username:{type: String, required:true},
password:{type: String, required:true},
name:{type: String, required:true},
mobileNumber:{type: Number, required:true}
});
module.exports=mongoose.model('userDetails',userDetailsSchema);