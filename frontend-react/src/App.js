import React, { Component } from 'react';
import MainComponent from './mainComponent.js'
import './App.css';


function App() {

  return (
    <div className="App">
      <MainComponent />
    </div>
  );
}

export default App;
