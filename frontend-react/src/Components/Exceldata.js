import React from "react";
import PropTypes from "prop-types";
import "./Exceldata.css";

function Exceldata(props) {
    return (
        <div onClick={(item)=>{console.log(item.target)}} className="excel" >
            <span>{props.filename}</span>
        
       </div>
    )

}
Exceldata.propTypes = {
    filename: PropTypes.string.isRequired
  };
export default Exceldata;