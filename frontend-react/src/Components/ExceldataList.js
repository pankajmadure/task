import React from "react";
import Exceldata from './Exceldata';

function ExceldataList(props) {
    return (
        <div>
            {props.exceldata.map(c =>
                <Exceldata key={c._v} filename={c.filename} />
            )}
        </div>
    );
}

export default ExceldataList;