import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import axios from 'axios';
import Modal from 'react-awesome-modal';
var config_server_url = "http://localhost:4000/"

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            visible: false

        }

    }
    handleClick() {
        axios.get(config_server_url + "api/getUser/" + this.state.username + "/" + this.state.password)
            .then(response => {
                if (response.status === 200) {
                    localStorage.setItem('userLoggedIn', true)
                    localStorage.setItem('username', response.data.username)
                    localStorage.setItem('password', response.data.password)
                    localStorage.setItem('id', response.data._id)
                    this.props.onSuccessfulLogin()
                }
            }).catch(err => {
                console.log("error ", err)
                this.openModal()
            })
    }
    openModal() {
        this.setState({
            visible: true,
            
        });
    }

    closeModal() {
        this.setState({
            visible: false
        });
    }

    render() {
        return (
            <div>
                <MuiThemeProvider>
                    <div>
                        <TextField
                            error={this.state.username.length === 0 ? false : true}
                            hintText="Enter your Username"
                            floatingLabelText="Username"
                            onChange={(event, newValue) => this.setState({ username: newValue })}
                        />
                        <br />
                        <TextField
                            validate
                            type="password"
                            hintText="Enter your Password"
                            floatingLabelText="Password"
                            onChange={(event, newValue) => this.setState({ password: newValue })}
                        />
                        <br />
                        <RaisedButton label="Login" primary={true} style={style} onClick={(event) => this.handleClick(event)} />
                        <Modal visible={this.state.visible} width="400" height="300" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                            <div>
                                <h1>Warning</h1>
                                <p>User does not exist or username and password are mismatched</p>
                                <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
                            </div>
                        </Modal>

                    </div>
                </MuiThemeProvider>
            </div>
        )
    }
}

const style = {
    margin: 15,
};

export default LoginScreen