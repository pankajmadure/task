import React, { Component } from 'react';
import Profile from './profileScreen';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FileOperations from './fileOperations';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import RaisedButton from 'material-ui/RaisedButton';

import "react-tabs/style/react-tabs.css";



export default class Profile_FileoperationsScreen extends Component {
    constructor(props) {
        super(props);
        this.LogoutFunctionality = this.LogoutFunctionality.bind(this);
    }

    LogoutFunctionality() {
        this.props.Logout();
    }
    render() {
        return (

            <MuiThemeProvider>

                <div>
                    <RaisedButton height = {"20"}label="Logout" primary={true} onClick={(event) => this.LogoutFunctionality(event)} />
                    <Tabs>
                        <TabList>
                            <Tab>
                                Profile
                                   </Tab>
                            <Tab>
                                FileOperations
                                    </Tab>

                        </TabList>
                        <TabPanel>
                            <Profile
                                Logout={() => {
                                    this.LogoutFunctionality()
                                }}
                            />
                        </TabPanel>

                        <TabPanel>
                            <FileOperations />
                        </TabPanel>



                    </Tabs>







                </div>

            </MuiThemeProvider>
        )
    }
}