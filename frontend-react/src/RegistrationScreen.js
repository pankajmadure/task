import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import axios from 'axios'
import Modal from 'react-awesome-modal';
import TextField from 'material-ui/TextField';
var config_server_url = "http://localhost:4000/"

export default class RegistrationScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            name: "",
            mobileNumber: "",
            visible: false
        }

    }
    handleClick() {
        var body = {}
        body.username = this.state.username;
        body.password = this.state.password;
        body.name = this.state.name;
        body.mobileNumber = this.state.mobileNumber;
        this.setState({
            username: "",
            password: "",
            name: "",
            mobileNumber: ""
        })
        if (body.username.length != 0 && body.password.length != 0 && body.name.length != 0 && body.mobileNumber.length != 0) {
            axios.post(config_server_url + "api/saveUser/", body)
                .then(response => {
                    if (response.status === 201) {
                        console.log("data is saved successfully please go to login page", response)
                        this.openModal()

                    }

                }).catch(error => {
                    console.log("error in storing data", error)
                })
        }
        else{
            console.log("please fill up the data")
        }

    }
    openModal() {
        this.setState({
            visible: true,
            username: "",
            password: "",
            name: "",
            mobileNumber: "",

        });
    }

    closeModal() {
        this.setState({
            visible: false
        });
    }
    render() {
        return (
            <div>
                <MuiThemeProvider>
                    <div>
                        <TextField
                            hintText="Enter your Username"
                            floatingLabelText="Username"
                            onChange={(event, newValue) => this.setState({ username: newValue })}
                        />
                        <br />
                        <TextField
                            type="password"
                            hintText="Enter your Password"
                            floatingLabelText="Password"
                            onChange={(event, newValue) => this.setState({ password: newValue })}
                        />
                        <br />
                        <TextField
                            hintText="Enter your Name"
                            floatingLabelText="Name"
                            onChange={(event, newValue) => this.setState({ name: newValue })}
                        />
                        <br />
                        <TextField
                            hintText="Enter your mobile number"
                            floatingLabelText="Mobile Number"
                            onChange={(event, newValue) => this.setState({ mobileNumber: newValue })}
                        />
                        <br />
                        <Modal visible={this.state.visible} width="400" height="300" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                            <div>
                                <h1>Sucess</h1>
                                <p>data is saved successfully please go to login page.....</p>
                                <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
                            </div>
                        </Modal>
                        <RaisedButton label="Register" primary={true} style={style} onClick={(event) => this.handleClick(event)} />

                    </div>
                </MuiThemeProvider>
            </div>
        )
    }
}

const style = {
    margin: 15,
};

