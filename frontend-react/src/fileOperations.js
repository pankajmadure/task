import React, { Component } from 'react';

import { OutTable, ExcelRenderer } from 'react-excel-renderer';
import "./Components/Exceldata.css";


import axios from 'axios';
var config_server_url = "http://localhost:4000/"
var username, password;



export default class fileOperations extends Component {
    constructor(props) {
        super(props);

        this.fileHandler = this.fileHandler.bind(this);
        this.state = {
            filename: "",
            cols: [],
            rows: [],
            data: null
        }
        this.handleClick = this.handleClick.bind(this);
      
    }

    handleClick(item) {
        
        console.log("dfsfsff", JSON.stringify(item));
        this.setState({
            rows: item.rows,
            cols: item.cols
        })



    }

  
    componentDidMount() {
        
        username = localStorage.getItem('username');
        password = localStorage.getItem('password');
        console.log("dfasfadsf",username,password);
        axios.get(config_server_url + "api/getExcelInfo/" + username + "/" + password)
            .then(response => {
                if (response.status === 200) {
                    console.log("data that is being recieved", JSON.stringify(response.data))

                    this.setState({
                        data: response.data

                    })
                }
                else if (response.status === 404)
                    console.log("user does not exist or username and password are mismatched")
            })

    }

    fileHandler = (event) => {
        let fileObj = event.target.files[0];


        //just pass the fileObj as parameter
        ExcelRenderer(fileObj, (err, resp) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log("filename", fileObj)
                console.log("fafadsfafaf", JSON.stringify(resp))
                var id = localStorage.getItem('id')
                var body = {};
                body.filename = fileObj.name;
                body.rows = resp.rows
                body.cols = resp.cols
                console.log("body", JSON.stringify(body))
                axios.post(config_server_url + "api/saveExcelInfo/" + username + "/" + password, body)
                    .then(response => {
                        if (response.status === 201) {
                            console.log("data is saved successfully please go to login page", response)
                            alert("data is saved successfully ");
                        }

                    }).catch(error => {
                        console.log("error in storing data", error)
                    })

            }
        });

    }


    render() {

        return (
            <div>
                {this.state.data === null

                    ?
                    <div>Loading...</div>
                    :

                    <div>
                        {this.state.data.map((item) => {
                            return (
                                <div className="excel" onClick={(event) => { this.handleClick(item) }}>
                                    <span>{item.filename}</span>
                                </div>
                            )
                        })}
                    </div>

                }
                <input type="file" onChange={this.fileHandler} style={{ "padding": "10px" }} />

                <OutTable data={this.state.rows} columns={this.state.cols} />
            </div>
        )



    }
}