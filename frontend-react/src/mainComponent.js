import React, { Component } from 'react';
import LoginScreen from './LoginScreen.js'
import RegistrationScreen from './RegistrationScreen.js'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import Profile_FileoperationsScreen from './Profile_FileoperationsScreen.js'
import "react-tabs/style/react-tabs.css";
export default class mainComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userLoggedIn: false
        }

    }

    componentWillMount() {
        var status = localStorage.getItem('userLoggedIn');
        if (status) {
            this.setState({
                userLoggedIn: true
            })
        }
        else {
            this.setState({
                userLoggedIn: false
            })
        }
    }

    render() {

        return (


            <div>
                {
                    this.state.userLoggedIn
                        ?
                        <div >
                            <Profile_FileoperationsScreen Logout={() => {
                                this.setState({
                                    userLoggedIn: false
                                })
                                localStorage.clear()
                            }} />
                        </div>
                        :
                        <Tabs >
                            <TabList>
                                <Tab>Login</Tab>
                                <Tab>Register</Tab>
                            </TabList>
                            <TabPanel>
                                <LoginScreen onSuccessfulLogin={() => {
                                    this.setState({
                                        userLoggedIn: true
                                    })
                                }} />
                            </TabPanel>
                            <TabPanel>
                                <RegistrationScreen />
                            </TabPanel>
                        </Tabs>



                }








            </div>
        )

    }
}