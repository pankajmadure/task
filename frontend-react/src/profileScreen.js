import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import axios from 'axios';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';


var username, password, id;

var config_server_url = "http://localhost:4000/"
export default class profileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            name: "",
            mobileNumber: "",
            id: ""
        }
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        username = localStorage.getItem('username')
        password = localStorage.getItem('password')
        id = localStorage.getItem('id');
    }
    componentWillMount() {
        console.log(username, password, id, "thdfkdafkdasjfk")
    }

    handleUpdate() {
        var body = {}
        body._id = this.state.id
        body.username = this.state.username
        body.password = this.state.password
        body.name = this.state.name
        body.mobileNumber = this.state.mobileNumber
        axios.put(config_server_url + "api/saveUser/" + id, body)
            .then(response => {
                console.log(response, "updated succesfully")
            })
    }
    handleDelete() {
        axios.delete(config_server_url + "api/deleteUser/" + id)
            .then(response => {
                console.log(response, "deleted successfully");
                this.props.Logout()
            }).catch(err => {
                console.log("problem in deletion")
            })
    }
    componentDidMount() {

        axios.get(config_server_url + "api/getUser/" + username + "/" + password)
            .then(response => {

                this.setState({
                    id: response.data._id,
                    username: response.data.username,
                    password: response.data.password,
                    name: response.data.name,
                    mobileNumber: response.data.mobileNumber
                })
            })
    }
    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <TextField
                        value={this.state.username}
                        floatingLabelText="Username"
                        onChange={(event, newValue) => this.setState({ username: newValue })}
                    />
                    <br />
                    <TextField
                        value={this.state.password}
                        type="password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"
                        onChange={(event, newValue) => this.setState({ password: newValue })}
                    />
                    <br />
                    <TextField
                        value={this.state.name}
                        hintText="Enter your Name"
                        floatingLabelText="Name"
                        onChange={(event, newValue) => this.setState({ name: newValue })}
                    />
                    <br />
                    <TextField
                        value={this.state.mobileNumber}
                        hintText="Enter your mobile number"
                        floatingLabelText="Mobile Number"
                        onChange={(event, newValue) => this.setState({ mobileNumber: newValue })}
                    />
                    <br />
                    <RaisedButton label="Update" primary={true} onClick={(event) => {
                        if (window.confirm("Do you want to update your profile ?"))
                            this.handleUpdate(event)
                    }} />
                    {`             `}
                    <RaisedButton label="Delete" color="secondary" onClick={(event) => {
                        if (window.confirm('Are you sure to delete profile ?'))
                            this.handleDelete(event)

                    }} />
                </div>

            </MuiThemeProvider>

        )

    }
}